package import_demo;

public class Import {

    public static void main(String[] args) {

        /* ImportierMich liegt im default package und kann aus anderen packages NICHT importiert werden. */
        //ImportierMich imported = new ImportierMich();

        /* StackDemo liegt in einem user defined package und kann importiert werden. */
        //StackDemo stackDemo = new StackDemo();

    }

}
