package objekte.verkettete_liste;

/**
 * Eine einfache verkettete Liste.
 */
public class VerketteteListe {

    Knoten start;
    int laenge;

    public VerketteteListe(Knoten start) {
        this.start = start;
        this.laenge = 1;
    }

    public VerketteteListe() {
    }

    /**
     * Gibt den Knoten am übergebenen Index zurück.
     *
     * @param index der Index, darf nur zwischen 0 und Länge der Liste sein.
     * @return den Knoten am Index
     */
    private Knoten knotenAnIndex(int index) {
        if (index < 0 || index >= laenge) throw new IndexOutOfBoundsException();

        Knoten current = start;
        for (int i = 0; i < index; i++) {
            current = current.nachfolger; // iteriere über die Knoten
        }
        return current; //wenn Schleife beendet, haben wir den gesuchten Knoten
    }

    /**
     * Hängt einen Knoten an das Ende der Liste.
     *
     * @param knoten Knoten, der angehängt werden soll.
     */
    public void knotenAnhaengen(Knoten knoten) {

        if (this.start == null) {
            start = knoten;
        } else {
            Knoten current = start;
            while (current.hatNachfolger()){
                current = current.nachfolger;
            }
            current.nachfolger = knoten;
        }
        laenge++;
    }

    /**
     * Fügt einen Knoten am übergebenen Index ein.
     *
     * @param index  Index, an dem eingefügt werden soll.
     * @param knoten Knoten, der eingefügt werden sol.
     */
    public void knotenEinfuegen(int index, Knoten knoten) {
        if (index < 0 || index >= laenge) throw new IndexOutOfBoundsException();

        laenge++;
        if (index == 0){
            Knoten alterStart = start;
            start = knoten;
            start.nachfolger = alterStart;
        } else {
            Knoten vorgaenger = start;
            for (int i = 0; i < index-1; i++) {
                vorgaenger = vorgaenger.nachfolger;
            }
            Knoten zuErsetzen = vorgaenger.nachfolger;
            vorgaenger.nachfolger = knoten; //neuen Knoten einfädeln
            knoten.nachfolger = zuErsetzen;
        }
    }

    /**
     * Entfernt den Knoten am übergebenen Index.
     *
     * @param index Index an dem gelöscht werden soll.
     */
    public void entferneKnotenAnIndex(int index) {
        if (index < 0 || index >= laenge) throw new IndexOutOfBoundsException();

        if (index == 0) {
            start = start.nachfolger;
        } else {
            Knoten current = start;
            for (int i = 0; i < index - 1; i++) {
                current = current.nachfolger;
            }
            current.nachfolger = current.nachfolger.nachfolger; //a-b-c wird zu a-c
        }
        laenge--;
    }

    /**
     * Entfernt den übergebenen Knoten aus der Liste.
     *
     * @param knoten Knoten, der entfernt werden soll
     */
    public void entferneKnoten(Knoten knoten) {
        Knoten current = start;

        if (current == knoten) {
            start = start.nachfolger;
            laenge--;
        } else {
            for (int i = 0; i < laenge && current != null; i++) {
                if (current.nachfolger == knoten) {
                    current.nachfolger = current.nachfolger.nachfolger; // aus a-b-c wird a-c
                    laenge--;
                    break;
                }
                current = current.nachfolger;
            }
        }
    }

    /**
     * Löscht alle Vorkommen des übergebenen Wertes in der Liste.
     *
     * @param wert der zu löschende Wert
     */
    public void entferneAlleKnotenMitWert(int wert) {

        /* entferne solange den start, bis er den wert nicht mehr hat */
        while (start != null && start.wert == wert) {
            start = start.nachfolger;
            laenge--;
        }

        if (start != null) {
            /* entferne alle Knoten mit dem Wert nach dem start-Knoten */
            Knoten vorgaenger = start;
            Knoten current = start.nachfolger;

            while (current != null) {
                if (current.wert == wert) {
                    vorgaenger.nachfolger = current.nachfolger;
                    laenge--;
                }
                vorgaenger = current;
                current = current.nachfolger;
            }
        }
    }

    /**
     * Gibt die Laenge der Liste zurück.
     *
     * @return laenge der Liste.
     */
    public int laenge() {
        return laenge;
    }

    public String toString() {

        Knoten current = start;
        String ausgabe = String.valueOf(start != null ? start.wert : " ");

        /* iteriere über alle Knoten und füge ihren Wert an den String an */
        while (current != null && current.hatNachfolger()) {
            current = current.nachfolger;
            ausgabe += ", " + current.wert;
        }

        return "[" + ausgabe + "]";
    }

    public static void main(String[] args) {
        VerketteteListe liste = new VerketteteListe();
        Knoten k1_1 = new Knoten(1);
        Knoten k2_2 = new Knoten(2);
        Knoten k3_1 = new Knoten(1);
        Knoten k4_3 = new Knoten(3);
        Knoten k5_1 = new Knoten(1);
        Knoten k6_55 = new Knoten(55);

        liste.knotenAnhaengen(k1_1);
        liste.knotenAnhaengen(k2_2);
        liste.knotenAnhaengen(k3_1);
        liste.knotenAnhaengen(k4_3);
        liste.knotenAnhaengen(k5_1);
        liste.knotenEinfuegen(6, k6_55);

        System.out.println(liste.knotenAnIndex(3).wert);
        System.out.println(liste.knotenAnIndex(0).wert);
        System.out.println(liste);

        liste.entferneKnotenAnIndex(0);
        System.out.println(liste);
        System.out.println(liste.laenge);
        liste.entferneAlleKnotenMitWert(1);
        System.out.println(liste);

    }

}
