package objekte.verkettete_liste;

public class Knoten {

    int wert;
    Knoten nachfolger;

    public Knoten(int wert, Knoten nachfolger) {
        this.wert = wert;
        this.nachfolger = nachfolger;
    }

    public Knoten(int wert) {
        this.wert = wert;
    }

    public boolean hatNachfolger(){
        return nachfolger != null;
    }

}
