package objekte.einstieg;

public class Koordinate {

    double x;
    double y;

    public Koordinate(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double euklidischerAbstand(Koordinate koordinate){
        return Math.sqrt((Math.pow((this.x - koordinate.x), 2) + Math.pow((this.y - koordinate.y), 2)));
    }

}
