package objekte.einstieg;

public class Main {

    public static void main(String[] args) {

        //Programmieren Aufgabe 2
        Person person = new Person("Lara Schmidt", "Hamburg");
        Hund bella = new Hund("Bella", 12, "Golden Retriever", person);
        Hund luna = new Hund("Luna", 6, "Border Collie", person);

        System.out.println("Hund: " + bella.name + ", Besitzer: " + bella.besitzer.name);
        System.out.println("Hund: " + luna.name + ", Besitzer: " + luna.besitzer.name);

        //Programmieren Aufgabe 3
        Koordinate a = new Koordinate(10.0, 9.5);
        Koordinate b = new Koordinate(14.5, 10.5);

        double abstand = a.euklidischerAbstand(b);
        System.out.println(abstand);

    }

}
