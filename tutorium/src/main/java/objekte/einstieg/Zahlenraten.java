package objekte.einstieg;

import java.util.Random;
import java.util.Scanner;

/**
 * Übung Scanner. Gelöst mit For-schleife.
 * Lösung auch möglich mit While-Schleife oder For-Schleife mit break;
 */
public class Zahlenraten {

    public static void main(String[] args) {

        Random random = new Random();
        int zufallsZahl = random.nextInt(101);
        int gerateneZahl = -1;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Rate meine geheime Zahl zwischen 0 und 100. Wie viele Versuche möchtest du?");
        int versuche = scanner.nextInt();
        System.out.printf("Du hast %s Versuche!%n", versuche);

        /* Schleife beenden, wenn versuche aufgebraucht oder Zahl erraten */
        for (int i = 0; i < versuche && gerateneZahl != zufallsZahl; i++) {
            gerateneZahl = scanner.nextInt();
            if (gerateneZahl < zufallsZahl) {
                System.out.println("Deine Zahl ist zu klein!");
            }
            if (gerateneZahl > zufallsZahl) {
                System.out.println("Deine Zahl ist zu groß!");
            }
            if (gerateneZahl == zufallsZahl) {
                System.out.println("Richtig geraten!");
            }
        }

    }

}
