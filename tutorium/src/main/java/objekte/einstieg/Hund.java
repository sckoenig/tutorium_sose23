package objekte.einstieg;

public class Hund {

    String name;
    int alter;
    String rasse;
    Person besitzer;

    public Hund(String name, int alter, String rasse, Person besitzer) {
        this.name = name;
        this.alter = alter;
        this.rasse = rasse;
        this.besitzer = besitzer;
    }

    public Hund(String name, int alter, String rasse) {
        this.name = name;
        this.alter = alter;
        this.rasse = rasse;
    }
}
