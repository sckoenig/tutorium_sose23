package objekte.interfaces.bsp_logger;

public interface Logger {

    /**
     * Loggt einen gegebenen Text.
     * @param string text, der geloggt werden soll.
     */
    void log(String string);

}
