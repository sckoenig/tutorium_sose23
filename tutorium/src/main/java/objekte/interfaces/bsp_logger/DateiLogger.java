package objekte.interfaces.bsp_logger;

import java.io.FileWriter;
import java.io.IOException;

public class DateiLogger implements Logger {
    @Override
    public void log(String string){
        try (FileWriter writer = new FileWriter("test", true)){
            writer.write(string+"\n");
        } catch(IOException e){
            //nothing
        }
    }
}
