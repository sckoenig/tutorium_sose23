package objekte.interfaces.bsp_logger;

public class KonsolenLogger implements Logger {

    @Override
    public void log(String string) {
        System.out.println(string);
    }
}
