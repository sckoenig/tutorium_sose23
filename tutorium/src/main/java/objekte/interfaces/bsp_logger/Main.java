package objekte.interfaces.bsp_logger;

import java.util.Scanner;

public class Main {



    public static void main(String[] args) {


        /* beide haben den statischen Typen Logger! */
        Logger dateiLogger = new DateiLogger();
        Logger konsolenLogger = new KonsolenLogger();

        String text = "Dieser Text soll geloggt werden.";

        /* Zur Laufzeit verhalten sich die beiden Objekte unterschiedlich (dynamischer Typ ist die instanziierte Klasse),
           obwohl sie den gleichen statischen Typ haben. */
        dateiLogger.log(text); //loggt in eine datei
        konsolenLogger.log(text); //loggt auf Konsole

        demo();
    }

    public static void demo(){

        Scanner scan = new Scanner(System.in);
        System.out.println("Wie möchtest du loggen? (K/D)");
        String type = scan.next();
        Logger logger = type.equals("K")? new KonsolenLogger() : new DateiLogger();
        System.out.println("Alles, was du tippst, wird geloggt, bis du 'ende' schreibst .. ");

        String line = scan.nextLine();
        while (!line.equals("ende")){
            logger.log(line);
            line = scan.nextLine();
        }
    }

}
