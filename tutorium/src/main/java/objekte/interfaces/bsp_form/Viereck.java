package objekte.interfaces.bsp_form;

public class Viereck implements Form {
    public Viereck(int breite, int hoehe) {
        this.breite = breite;
        this.hoehe = hoehe;
    }

    private int breite;
    private int hoehe;

    @Override
    public void zeichnen() {
        System.out.println("*   *   *   *");
        System.out.println("*           *");
        System.out.println("*           *");
        System.out.println("*           *");
        System.out.println("*   *   *   *");
        System.out.println();
    }

    @Override
    public double berechneFlaeche() {
        return breite*hoehe;
    }
}
