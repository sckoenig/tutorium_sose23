package objekte.interfaces.bsp_form;

public class Dreieck implements Form {

    private int grundseite;
    private int hoehe;

    public Dreieck(int grundseite, int hoehe) {
        this.grundseite = grundseite;
        this.hoehe = hoehe;
    }

    @Override
    public void zeichnen() {
        System.out.println("      *    ");
        System.out.println("    *   *  ");
        System.out.println("  *       *");
        System.out.println("*   *   *   *");
        System.out.println();
    }

    @Override
    public double berechneFlaeche() {
        return grundseite*hoehe / 2.0;
    }

    public boolean istRechtwinklig(){
        return true; //fuer Demo
    }

}
