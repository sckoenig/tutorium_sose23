package objekte.interfaces.bsp_form;

public interface Form {

    void zeichnen();

    double berechneFlaeche();

}
