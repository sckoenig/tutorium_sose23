package objekte.menge;

import java.util.Objects;

public class MengenElement {

    private final String name;
    private final int wert;

    public MengenElement(String name, int wert) {
        this.name = name;
        this.wert = wert;
    }

    /**
     * Kopierkonstruktor.
     * @param element Element, das kopiert werden soll.
     */
    public MengenElement(MengenElement element) {
        this.name = element.getName();
        this.wert = element.getWert();
    }

    public String getName() {
        return name;
    }

    public int getWert() {
        return wert;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        MengenElement anderesObjekt = (MengenElement) o;
        return wert == anderesObjekt.wert && name.equals(anderesObjekt.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, wert);
    }

    @Override
    public String toString(){
        return String.format("(%s:%s)", name, wert);
    }
}
