package objekte.menge;

public class Main {

    public static void main(String[] args) {

        MengenElement a1 = new MengenElement("a", 1);
        MengenElement b2 = new MengenElement("b", 2);
        MengenElement c3 = new MengenElement("c", 3);
        MengenElement d4 = new MengenElement("d", 4);
        MengenElement d5 = new MengenElement("d", 5);
        MengenElement c4 = new MengenElement("c", 4);
        MengenElement c4_2 = new MengenElement("c", 4);

        Menge menge1 = new Menge();
        menge1.hinzufuegen(a1);
        menge1.hinzufuegen(b2);
        menge1.hinzufuegen(c3);
        menge1.hinzufuegen(d4);
        System.out.println(menge1);

        Menge menge2 = new Menge();
        menge2.hinzufuegen(a1);
        menge2.hinzufuegen(c4);
        menge2.hinzufuegen(c3);
        menge2.hinzufuegen(d5);
        System.out.println(menge2);
        System.out.println(menge2.enthaelt(c4_2));
        System.out.println(menge2.enthaelt(c4));
        System.out.println("Schnitt: " + menge1.schneiden(menge2));
        System.out.println("Vereinigen: " + menge1.vereinigen(menge2));
        System.out.println("Differenz: " + menge1.differenz(menge2));


        System.out.println("Menge2: "+menge2);
        menge2.entfernen(c4_2);
        System.out.println("c:4 entfernt:" +menge2);
        menge2.entfernen(c3);
        System.out.println("c:3 entfernt:" +menge2);
        menge2.entfernen(a1);
        System.out.println("a:1 entfernt:" +menge2);


    }

}
