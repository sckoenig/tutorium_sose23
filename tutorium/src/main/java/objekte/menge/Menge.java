package objekte.menge;

import java.util.Arrays;

public class Menge {

    private MengenElement[] elemente;

    public Menge() {
        this.elemente = new MengenElement[0];
    }

    public Menge(MengenElement[] elemente) {
        this.elemente = elemente;
    }

    public MengenElement[] getElemente() {
        return Arrays.copyOf(elemente, elemente.length);
    }

    public boolean hinzufuegen(MengenElement neuesElement) {
        if (neuesElement == null) throw new IllegalArgumentException("Element darf nicht null sein!");
        boolean ergebnis = false;

        if (!enthaelt(neuesElement)) {
            elemente = Arrays.copyOfRange(elemente, 0, elemente.length + 1);
            elemente[elemente.length - 1] = neuesElement;
            ergebnis = true;
        }
        return ergebnis;
    }

    public boolean enthaelt(MengenElement element) {
        for (MengenElement enthaltenesElement : elemente) {
            if (enthaltenesElement.equals(element)) {
                return true;
            }
        }
        return false;
    }

    public boolean entfernen(MengenElement element) {
        int indexDesElements = -1;
        for (int i = 0; i < elemente.length; i++) {
            if (elemente[i].equals(element)) {
                indexDesElements = i;
                break;
            }
        }
        if (indexDesElements > -1) {
            // (A,1)(B,2)(C,3) wird zu (A,1)(C,3)
            System.arraycopy(elemente, indexDesElements + 1, elemente, indexDesElements, elemente.length - 1-indexDesElements);
            elemente = Arrays.copyOfRange(elemente, 0, elemente.length - 1);
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        String string = "";
        for (MengenElement element : elemente) {
            string += element.toString();
        }
        return "{" + string + "}";
    }

    public Menge vereinigen(Menge andereMenge) {
        if (andereMenge == null) throw new IllegalArgumentException("Menge darf nicht null sein!");

        MengenElement[] menge = Arrays.copyOf(elemente, elemente.length);
        Menge vereinigungsMenge = new Menge(menge);

        for (MengenElement elem : andereMenge.getElemente()) {
            vereinigungsMenge.hinzufuegen(elem);
        }
        return vereinigungsMenge;
    }

    public Menge schneiden(Menge andereMenge){
        if (andereMenge == null) throw new IllegalArgumentException("Menge darf nicht null sein!");

        Menge schnittMenge = new Menge();

        for (MengenElement elem : elemente){
            if (andereMenge.enthaelt(elem)){
                schnittMenge.hinzufuegen(elem);
            }
        }
        return schnittMenge;
    }

    public Menge differenz(Menge andereMenge){
        if (andereMenge == null) throw new IllegalArgumentException("Menge darf nicht null sein!");

        Menge differenzMenge = new Menge();

        for (MengenElement elem : elemente){
            if (!andereMenge.enthaelt(elem)){
                differenzMenge.hinzufuegen(elem);
            }
        }
        return differenzMenge;
    }

}
