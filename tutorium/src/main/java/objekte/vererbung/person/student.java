package objekte.vererbung.person;

class Student extends Person{
    private String mtrkNr;
    //private String name;

    public Student(String name, String mtrkNr){
        super(name);
        this.mtrkNr=mtrkNr;
    }
    public String toString(){
        return String.format("Student: %s, Matrikelnummer: %s", getName(), mtrkNr);
        //return String.format("Student: %s, Matrikelnummer: %s", name, mtrkNr);
    }
}

