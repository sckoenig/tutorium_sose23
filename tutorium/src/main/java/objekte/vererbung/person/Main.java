package objekte.vererbung.person;

public class Main {

    public static void main(String[] args) {
        Person tim = new Student("Tim", "14791");

        System.out.println(tim);
        System.out.println(tim.getName());
        tim.setName("Tim Schmidt");
        System.out.println(tim);
        System.out.println(tim.getName());

        Object a = "Hello";
        a = new int[4];
        System.out.println(a.getClass());
    }
}


