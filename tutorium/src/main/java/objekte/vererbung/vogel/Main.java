package objekte.vererbung.vogel;

public class Main {

    public static void main(String[] args) {

        Vogel amsel = new Amsel("Amsel");
        KannSchwimmen ente = new Ente();

        System.out.println(amsel);
        System.out.println(ente);
        ente.schwimmen();

    }

}
