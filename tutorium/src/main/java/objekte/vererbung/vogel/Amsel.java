package objekte.vererbung.vogel;

public class Amsel extends Vogel {

    public Amsel(String vogelArt) {
        super(vogelArt);
    }

    public String getVogelArt(){
        return "Dies ist eine Amsel";
    }
}
