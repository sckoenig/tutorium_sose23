package objekte.vererbung.vogel;

public class Ente extends Vogel implements KannSchwimmen {

    public Ente() {
        super();
    }

    @Override
    public String getVogelArt(){
        return "Ente";
    }

    @Override
    public void schwimmen() {
        System.out.printf("Die %s schwimmt. %n", super.getVogelArt());
    }
}
