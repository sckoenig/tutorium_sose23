package objekte.vererbung.vogel;

public abstract class Vogel {

    protected String vogelArt;

    protected Vogel() {
        this("abstract");
    }

    protected Vogel(String vogelArt) {
        this.vogelArt = vogelArt;
        System.out.printf("Vogel der Art %s erzeugt! %n", vogelArt);
    }

    public String getVogelArt() {
        return "Vogel: "+ vogelArt;
    }

    @Override
    public String toString() {
        return getVogelArt();
    }
}
