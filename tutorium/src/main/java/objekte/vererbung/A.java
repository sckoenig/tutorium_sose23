package objekte.vererbung;

class A{
    public void m1(){
        System.out.println("A:m1");
        m2();
    }
    private void m2(){ System.out.println("A:m2"); }
}
