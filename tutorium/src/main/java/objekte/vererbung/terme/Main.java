package objekte.vererbung.terme;

public class Main {

    public static void main(String[] args) {

        AtomarerTerm a2 = new AtomarerTerm(2.0);
        AtomarerTerm a15 = new AtomarerTerm(1.5);
        ZusammengesetzterTerm a = new ZusammengesetzterTerm(ZusammengesetzterTerm.Operator.GETEILT, a2, a15);
        System.out.println(a);
        System.out.println(a.berechneWert());

        AtomarerTerm b73= new AtomarerTerm(7.3);
        ZusammengesetzterTerm b = new ZusammengesetzterTerm(ZusammengesetzterTerm.Operator.PLUS, b73, a);
        System.out.println(b);
        System.out.println(b.berechneWert());


        AtomarerTerm c45= new AtomarerTerm(4.5);
        AtomarerTerm c2= new AtomarerTerm(2.0);
        AtomarerTerm c10 = new AtomarerTerm(10.0);
        AtomarerTerm c15 = new AtomarerTerm(10.5);
        ZusammengesetzterTerm ca = new ZusammengesetzterTerm(ZusammengesetzterTerm.Operator.MAL, c45, c2);
        ZusammengesetzterTerm cb = new ZusammengesetzterTerm(ZusammengesetzterTerm.Operator.GETEILT, ca, c10);
        ZusammengesetzterTerm c = new ZusammengesetzterTerm(ZusammengesetzterTerm.Operator.PLUS, cb, c15);
        System.out.println(c);
        System.out.println(c.berechneWert());

    }

}
