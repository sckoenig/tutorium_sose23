package objekte.vererbung.terme;

public class ZusammengesetzterTerm extends Term {

    public enum Operator {

        PLUS, MINUS, MAL, GETEILT;

        public String toString() {
            String ausgabe = "";
            switch (this) {
                case PLUS:
                    ausgabe = "+";
                    break;
                case MAL:
                    ausgabe = "*";
                    break;
                case MINUS:
                    ausgabe = "-";
                    break;
                case GETEILT:
                    ausgabe = "/";
                    break;
            }
            return ausgabe;
        }
    }

    private Operator operator;
    private Term[] operanden;

    public ZusammengesetzterTerm(Operator operator, Term operand1, Term operand2) {
        this.operator = operator;
        this.operanden = new Term[]{operand1, operand2};
    }

    @Override
    public double berechneWert() {
        Term operand1 = operanden[0];
        Term operand2 = operanden[1];
        switch (operator) {
            case PLUS:
                return operand1.berechneWert() + operand2.berechneWert();
            case MAL:
                return operand1.berechneWert() * operand2.berechneWert();
            case MINUS:
                return operand1.berechneWert() - operand2.berechneWert();
            case GETEILT:
                return operand1.berechneWert() / operand2.berechneWert();
            default:
                return 0.0;
        }
    }

    @Override
    public String toString(){
        return "(" + operanden[0].toString() + " " + operator.toString() + " "+ operanden[1].toString() + ")";
    }

}
