package objekte.vererbung.terme;

public class AtomarerTerm extends Term {

    public static final double GENAUIGKEIT = 0.001;

    private double wert;

    public AtomarerTerm(double wert) {
        this.wert = wert;
    }

    @Override
    public double berechneWert() {
        return wert;
    }

    @Override
    public String toString(){
        return String.format("%.02f", wert);
    }

    @Override
    public boolean equals(Object obj){
        if (obj == null  || this.getClass() != obj.getClass()) return false;
        double andererWert = ((AtomarerTerm) obj).berechneWert();
        return Math.abs(this.wert - andererWert) <= GENAUIGKEIT;
    }

}
