package objekte.uebung_objekte_bibliothek;

/**
 * Simuliert eine Person.
 */
public class Person {

    public String toString(){
        return String.format("%s, %s", name, adresse);
    }

    String name;
    String adresse;

    public Person(String name, String adresse) {
        this.name = name;
        this.adresse = adresse;
    }

    public Person(String name) {
        this.name = name;
        this.adresse = "adresse";
    }

    /**
     * Verändert die Adresse dieser Person.
     * @param neueAdresse die neue Adresse
     */
    public void changeAdresse(String neueAdresse){
        this.adresse = neueAdresse;
    }
}
