package objekte.uebung_objekte_bibliothek;

/**
 * Simuliert eine Bibliothek, die eine feste Anzahl an Büchern zur Ausleihe enthalten kann.
 */
public class Bibliothek {

    Buch[] buecher;
    int buecherZaehler;
    int maxBuecher;

    public Bibliothek(int maxBuecher){
        this.maxBuecher = maxBuecher;
        this.buecherZaehler = 0;
        this.buecher = new Buch[maxBuecher];
    }

    /**
     * Fügt ein {@link Buch} der Bibliothek hinzu.
     * @param buch Buch, das hinzugefügt werden soll.
     */
    public void fuegeBuchHinzu(Buch buch){
        if (buecherZaehler != maxBuecher) {
            this.buecher[buecherZaehler++] = buch;
        }
    }

    /**
     * Gibt alle Bücher der Bibliothek mit Namen und Ausleihstatus auf der Konsole aus.
     */
    public void gibAlleBuecher(){
        for (int i = 0; i < buecherZaehler; i++) {
            System.out.println(buecher[i].name + ", ausgeliehen: "+ (buecher[i].ausgeliehen? "ja" : "nein"));
        }
    }

    /**
     * Vermerkt in einem {@link Buch}, dass eine {@link Person} es ausgeliehen hat.
     * @param buch Buch, das ausgeliehen wird
     * @param person Person, die das Buch ausleiht
     */
    public void ausleihen(Buch buch, Person person){
        if (!buch.ausgeliehen){
            buch.ausgeliehen = true;
            buch.ausleiher = person;
            System.out.println(person.name + " hat "+buch.name + " ausgeliehen.");
        }
        else System.out.println("Das Buch ist schon ausgeliehen!");
    }

    /**
     * Vermerkt in einem {@link Buch}, dass es nicht mehr ausgeliehen ist.
     * @param buch Buch, das zurückgegeben wird.
     */
    public void zurueckgeben(Buch buch){
        buch.ausleiher = null;
        buch.ausgeliehen = false;
    }

}
