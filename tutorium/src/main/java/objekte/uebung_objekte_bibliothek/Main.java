package objekte.uebung_objekte_bibliothek;

public class Main {

    public static void main(String[] args) {
        //a
        Buch buch1 = new Buch("Name of the Wind", "1234");
        Buch buch2 = new Buch("Lord of the Rings", "33434");
        Buch buch3 = new Buch("Mistborn", "54634");
        System.out.println(buch1.name);

        //b
        Bibliothek bibliothek = new Bibliothek(3);

        //c
        bibliothek.fuegeBuchHinzu(buch1);
        bibliothek.fuegeBuchHinzu(buch2);
        bibliothek.fuegeBuchHinzu(buch3);

        //d
        Buch buch4 = new Buch("Harry Potter", "234334");
        bibliothek.fuegeBuchHinzu(buch4);

        //e
        bibliothek.gibAlleBuecher();

        //f
        Person tim = new Person("Tim", "... Hamburg");
        Person anna = new Person("Anna", "... Dresden");

        //g
        System.out.println(tim.adresse);

        //h
        tim.changeAdresse("... Hannover");
        System.out.println(tim.adresse);

        //i
        bibliothek.ausleihen(buch1, tim);
        bibliothek.ausleihen(buch3, anna);
        bibliothek.gibAlleBuecher();
        bibliothek.zurueckgeben(buch3);


    }

}
