package objekte.uebung_objekte_bibliothek;

/**
 * Simuliert ein Buch.
 */
public class Buch {

    String name;
    boolean ausgeliehen;
    Person ausleiher;
    String isbn;

    public Buch(String name) {
        this.name = name;
    }

    public Buch(String name, String isbn) {
        this.name = name;
        this.ausgeliehen = false;
        this.ausleiher = null;
        this.isbn = isbn;
    }

}
