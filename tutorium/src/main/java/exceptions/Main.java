package exceptions;

public class Main {

    public static void main(String[] args)  {

        int eingabe = -1; //Benutereingabe;

        // hier sicher sum(eingabe) aufrufen:

        tryCatch(2);
        tryCatch(4);

        methode1();
    }


    public static int sum(int n) throws UngueltigeZahlException {
        if (n < 0) throw new UngueltigeZahlException("n muss >= 0 sein!");
        int summe = 0;
        for (int i = 0; i < n; i++) {
            summe += i;
        }
        return summe;
    }

    public static void tryCatch(int i){

        int[] array = new int[]{1,2,3,4};
        try{
            array[i] = 999;
            System.out.println("Array wurde verändert!");
        } catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Index ist out of bounds!");
        } finally {
            System.out.println("Immer richtigen Index verwenden!");
        }
        System.out.println("Ende!");
    }

    public static void methode1() {
        //methode2();
    }

    public static void methode2() throws Exception {
        throw new Exception("Exception in methode2");
    }

}
