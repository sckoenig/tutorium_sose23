package exceptions;

public class UngueltigeZahlException extends Exception {

    public UngueltigeZahlException(String nachricht){
        super(nachricht);
    }
}
