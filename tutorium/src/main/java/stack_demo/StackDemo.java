package stack_demo;

public class StackDemo {

    public static void main(String[] args) throws Exception {
        int localVariable = 0;
        methodOne(10);
        methodThree();
    }

    public static void methodOne(int x){
        int localVariable = 10+x;
        methodTwo(localVariable);
    }

    public static void methodTwo(int y){
        int localVariable = 15;
        System.out.println(localVariable);
    }

    public static void methodThree() throws Exception {
        int localVariable = 20;
        throw new Exception("Demo");
    }

}
