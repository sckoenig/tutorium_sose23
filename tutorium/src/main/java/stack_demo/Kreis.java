package stack_demo;

public class Kreis {

    int radius;

    public Kreis(int radius) {
        this.radius = radius;
    }

    public Kreis(){
        this(1);
    }

    public void vergroessern(int zahl){
        this.radius += zahl;
        System.out.println("Neuer Radius: " + radius);
    }

    public static void main(String[] args) {

        Kreis kreis1 = new Kreis();
        System.out.println(kreis1.radius);
        Kreis kreis = new Kreis(10);
        System.out.println(kreis.radius);
        Kreis kreis2 = new Kreis(20);
        kreis.vergroessern(kreis2.radius);

    }
}
