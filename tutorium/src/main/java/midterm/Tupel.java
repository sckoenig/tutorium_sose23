package midterm;

class Tupel {

    private int zahl;
    private char zeichen;

    public Tupel(int zahl, char zeichen){
        this.zahl = zahl;
        this.zeichen = zeichen;
    }

    public int getZahl() {
        return zahl;
    }

    public int getZeichen() {
        return zeichen;
    }

    public Tupel zusammen(Tupel t){
        if (t == null) return new Tupel(this.zahl, this.zeichen);
        return new Tupel(this.zahl + t.zahl, this.zeichen < t.zeichen? this.zeichen : t.zeichen);
    }

    public void flip(){
        int neueZahl = this.zeichen;
        this.zeichen = (char) (this.zahl < 0? 0 : Math.min(this.zahl, 127));
        this.zahl = neueZahl;
    }

    public String toString(){
        return "midterm.Tupel("+this.zahl + ", " + (int) this.zeichen + ")";
    }
}