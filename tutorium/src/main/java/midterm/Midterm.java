package midterm;

public class Midterm {

    public static void main(String[] args) {

        System.out.println(summeGeraderZahlen(5,10));
        System.out.println(summeGeraderZahlen(-4,3));
        System.out.println(gleitkomma2Kleinbuchstabe(-0.5));
        System.out.println(gleitkomma2Kleinbuchstabe(25.4));
        System.out.println(gleitkomma2Kleinbuchstabe(17.7));
        System.out.println(rechner('+',2,3));
        System.out.println(rechner('-',5,1));
        System.out.println(rechner('h',23,42));

        Tupel t1 = new Tupel(23, 'a');
        Tupel t2 = new Tupel(42, 'c');
        Tupel t3 = new Tupel(-2, 'k');
        Tupel t4 = new Tupel(3, 'b');
        System.out.println(t1.zusammen(t2));
        System.out.println(t3.zusammen(t4));

        Tupel t5 = new Tupel(177, 'a');
        Tupel t6 = new Tupel(-1, 'c');
        t5.flip();
        t6.flip();
        System.out.println(t5);
        System.out.println(t6);

    }

    private static int summeGeraderZahlen(int start, int ende) {
        int summe = 0;
        start += (start%2); //starte bei gerader zahl und zähle +2 bis ende.
        for (int i = start; i <= ende ; i+=2) {
            summe += i;
        }
        return summe;
    }

    public static char gleitkomma2Kleinbuchstabe(double wert){
        if (wert < -0.5 ||wert > 25.4) throw new IllegalArgumentException();
        return (char) (97 + Math.round(wert)); //97=a
    }

    public static int rechner(char operator, int operand1, int operand2){
        switch(operator){
            case '+': return operand1 + operand2;
            case '-': return operand1 - operand2;
            default: return 0;
        }

    }

}

