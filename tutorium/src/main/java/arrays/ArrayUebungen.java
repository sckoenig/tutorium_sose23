package arrays;

import java.util.Arrays;

public class ArrayUebungen {

    public static void main(String[] args) {

        System.out.println(Arrays.toString(geradeZahlenArray(9)));
        System.out.println(Arrays.toString(geradeZahlenArray(10)));

        System.out.println(istEnthalten(new int[]{1, 2, 3, 2, 1}, 1));
        System.out.println(istEnthalten(new int[]{1, 2, 3, 2, 1}, 5));
        System.out.println(durchschnitt(new int[]{1, 2, 3, 2, 1}));
        System.out.println(durchschnitt(new int[]{1, 2, 3, 2}));
        System.out.println(Arrays.toString(umdrehen(new int[]{1, 2, 3, 4})));

        System.out.println(Arrays.toString(palindrom(new int[]{1, 2, 3, 4})));
        System.out.println(Arrays.toString(palindrom(new int[]{3, 5, 7})));
        System.out.println(Arrays.toString(loeschen(new int[]{1, 2, 1, 2, 1, 5}, 1)));
        System.out.println(Arrays.toString(loeschen(new int[]{1, 2, 1, 2, 1, 5}, 2)));

    }

    private static void notNull(int[] array){
        if (array == null) throw new IllegalArgumentException("Array darf nicht null sein!");
    }

    public static int[] geradeZahlenArray(int n) {
        if (n<0) throw new IllegalArgumentException("n muss > 0 sein");

        int[] ergebnis = new int[Math.abs(n) / 2 + 1]; //+1 für die 0

        int counter = 0;
        for (int i = 0; i <= n; i += 2) {
            ergebnis[counter++] = i;
        }

        return ergebnis;
    }

    public static boolean istEnthalten(int[] array, int zahl) {
        notNull(array);

        for (int zahlInArray : array) {
            if (zahlInArray == zahl) return true;
        }
        return false;
    }

    public static double durchschnitt(int[] array) {
        notNull(array);

        double summe = 0;
        for (int zahl : array) {
            summe += zahl;
        }
        return summe / array.length;
    }

    public static int[] umdrehen(int[] array) {
        notNull(array);

        int[] ergebnis = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            ergebnis[array.length - i - 1] = array[i];
        }
        return ergebnis;

    }

    public static int[] palindrom(int[] array) {
        notNull(array);

        int[] ergebnis = Arrays.copyOf(array,  array.length * 2);
        System.arraycopy(umdrehen(array), 0, ergebnis, array.length, array.length);
        return ergebnis;
    }

    public static int[] loeschen(int[] array, int zahl) {
        notNull(array);

        int[] ergebnis = Arrays.copyOf(array, array.length);

        int counter = 0;
        for (int zahlInArray : array) {
            if (zahlInArray != zahl){
                ergebnis[counter++] = zahlInArray;
            }
        }

        return Arrays.copyOfRange(ergebnis, 0, counter);
    }

}
