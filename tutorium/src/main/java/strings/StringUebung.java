package strings;

public class StringUebung {


    /**
     * Prüft, ob der übergebene String im Character-Array enthalten ist, bsp:
     * ['a', 'k', 'm', 'i', 'k', 'e', 'z'], "mike" ist true.
     * @param chars character array
     * @param string text, auf den geprüft werden soll
     * @return true, wenn der text im Array enthalten ist
     * @throws IllegalArgumentException wenn das Array null ist, oder der String null ist.
     */
    public static boolean istEnthalten(char[] chars, String string){
        if (chars == null) throw new IllegalArgumentException("Array darf nicht null sein!");
        if (string == null) throw new IllegalArgumentException("String darf nicht null sein!");

        String temp = String.valueOf(chars);
        return temp.contains(string);
    }

}
