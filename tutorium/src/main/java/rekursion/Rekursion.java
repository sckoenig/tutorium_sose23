package rekursion;

public class Rekursion {

    public static void main(String[] args) {
        System.out.println(summeBisNIterativ(5));
        System.out.println(summeBisNRekursiv(5));
        System.out.println(summeBisNEndRekursiv(5, 1));
        System.out.println(erzeugeAlphabetRekursiv());
        System.out.println(erzeugeAlphabetIterativ());
        System.out.println(summeGeraderZahlenRekursiv(8));
        System.out.println(summeGeraderZahlenRekursiv(9));

    }

    public static int summeBisNIterativ(int n) {
        int summe = 0;
        for (int i = 1; i <= n; i++) {
            summe += i;
        }
        return summe;
    }

    /* REKURSION: Berechnung erfolgt erst beim rekursiven Aufstieg.
    * summeBisNRekursiv(5)
	        5 + summeBisNRekursiv(4)
		        4 + summeBisNRekursiv(3)
			        3 + summeBisNRekursiv(2)
				        2 + summeBisNRekursiv(1)
			            2 + 1
			        3 + 3
		        4 + 6
    return  5 + 10
    */
    public static int summeBisNRekursiv(int n) {
        if (n <= 1) return 1;
        return n + summeBisNRekursiv(n - 1);
    }

    /* ENDREKURSION: Berechnung erfolgt schon beim rekursiven Abstieg.
    summeBisNEndRekursiv(5, 1)
	        summeBisNRekursiv(4, 1 + 5)
		        summeBisNRekursiv(3, 6 + 4)
			        summeBisNRekursiv(2, 10 + 3)
				        summeBisNRekursiv(1, 13 + 2)
				        15
			        15
		        15
    return	15
    */
    public static int summeBisNEndRekursiv(int n, int summe) {
        if (n <= 1) return summe;
        return summeBisNEndRekursiv(n - 1, n + summe);
    }

    public static String erzeugeAlphabetIterativ(){
        String ergebnis = "";
        for (char i = 'a'; i <= 'z'; i++) {
            ergebnis += i;
        }
        return ergebnis;
    }

    public static String erzeugeAlphabetRekursiv(){
        return erzeugeAlphabetRekursiv('a', "");
    }

    private static String erzeugeAlphabetRekursiv(char aktuell, String ergebnis){
        if (aktuell > 'z') return ergebnis;
        return erzeugeAlphabetRekursiv((char) (aktuell+1), ergebnis+aktuell);
    }

    public static int summeGeraderZahlenRekursiv(int n){
        if (n % 2 != 0) throw new IllegalArgumentException("n muss gerade sein!");

        if (n == 0) return 0;
        return n + summeGeraderZahlenRekursiv(n-2);
    }

}
