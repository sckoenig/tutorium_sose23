package strings;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringUebungTest {

    /*
    * Aequivalenzklassen:
    * AEK1 => gueltig: charArray not null, String in charArray enthalten -> true
    * AEK2 => gueltig: charArray not null, String nicht enthalten -> false
    * AEK3 => ungueltig: charArray null, String not null -> Illegal
    * AEK4 => ungueltig: charArray not null, String null -> Illegal
    * */

    /* Testmethoden haben oft einen standardisierten Aufbau:
     *       - Given: Ausgangslage, bevor zu testende Methode aufgerufen wird.
     *       - When: Aufruf der zu testenden Methode
     *       - Then: Lage nach Aufruf der Methode -> entspricht dies den Erwartungen?
     *
     *       - Gegeben (Given) eine definierte Ausgangslage: Wenn (When) eine Aktion / Methode ausgeführt wird, dann (Then) wird ein definiertes Ergebnis erwartet.
     *       - Given a specific condition, when a certain action is performed, then a particular result is expected.
     * */

    @Test
    public void istEnthaltenAEK1(){
        //Given
        char[] array = new char[] {'a', 'k', 'm', 'i', 'k', 'e', 'z'};
        String string = "mike";

        //When
        boolean ergebnis = StringUebung.istEnthalten(array, string);

        //Then
        assertTrue(ergebnis);
    }

    @Test
    public void istEnthaltenAEK1Randfall(){
        //Given
        char[] array = new char[] {'a', 'k', 'm', 'i', 'k', 'e', 'z'};
        String string = "";

        //When
        boolean ergebnis = StringUebung.istEnthalten(array, string);

        //Then
        assertTrue(ergebnis);
    }

    @Test
    public void istEnthaltenAEK2(){
        //Given
        char[] array = new char[] {'a', 'k', 'm', 'i', 'k', 'e', 'z'};
        String string = "mikey";

        //When
        boolean ergebnis = StringUebung.istEnthalten(array, string);

        //Then
        assertFalse(ergebnis);
    }

    @Test
    public void istEnthaltenAEK3(){
        //Given
        char[] array = null;
        String string = "mikey";

        //When and Then
        assertThrows(IllegalArgumentException.class, () -> StringUebung.istEnthalten(array, string));
    }

    @Test
    public void istEnthaltenAEK4(){
        //Given
        char[] array = new char[] {'a', 'k', 'm', 'i', 'k', 'e', 'z'};
        String string = null;

        //When and Then
        assertThrows(IllegalArgumentException.class, () -> StringUebung.istEnthalten(array, string));
    }


}
