package exceptions;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.io.IOException;

import static exceptions.Main.sum;
import static org.junit.jupiter.api.Assertions.*;

public class TestenAufExceptions {


    @Test
    public void tryCatchTest(){
        try{
            int sum = sum(-1);
            assertTrue(false);
        } catch( UngueltigeZahlException e){
            //exception wurde geworfen -> Test erfolgreich!
        }
    }

    @Test
    public void lambdaTest(){
        Assertions.assertThrows(UngueltigeZahlException.class, () -> sum(-1));
    }

}
