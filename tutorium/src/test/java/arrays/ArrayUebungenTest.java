package arrays;

import org.junit.Test;

import static org.junit.Assert.*;

public class ArrayUebungenTest {

    /* Testmethoden haben oft einen standardisierten Aufbau:
     *       - Given: Ausgangslage, bevor zu testende Methode aufgerufen wird.
     *       - When: Aufruf der zu testenden Methode
     *       - Then: Lage nach Aufruf der Methode -> entspricht dies den Erwartungen?
     *
     *       - Gegeben (Given) eine definierte Ausgangslage: Wenn (When) eine Aktion / Methode ausgeführt wird, dann (Then) wird ein definiertes Ergebnis erwartet.
     *       - Given a specific condition, when a certain action is performed, then a particular result is expected.
     * */

    @Test
    public void geradeZahlenArrayTestEvenN(){
        //Given / Ausgangslage
        int n = 10;

        //When / Aufruf
        int[] result = ArrayUebungen.geradeZahlenArray(n);

        //Then / Lage nach Aufruf
        int[] expectedResult = new int[] {0, 2, 4, 6, 8, 10};
        assertArrayEquals(result, expectedResult);
    }

    @Test
    public void geradeZahlenArrayTestUnevenN(){
        //Given
        int n = 15;

        //When
        int[] result = ArrayUebungen.geradeZahlenArray(n);

        //Then
        int[] expectedResult = new int[] {0, 2, 4, 6, 8, 10, 12, 14};
        assertArrayEquals(result, expectedResult);
    }

    @Test
    public void geradeZahlenArrayTestNegativeN(){
        //Given
        int n = -15;

        //When and Then
        assertThrows(IllegalArgumentException.class, () -> ArrayUebungen.geradeZahlenArray(n));
    }

    @Test
    public void istEnthaltenTestNotNull(){
        //Given
        int[] testArray = new int[]{1, 2, 3, 2, 1};
        int zahl = 1;

        //When
        boolean result = ArrayUebungen.istEnthalten(testArray, zahl);

        //Then
        assertTrue(result);
    }

    @Test
    public void istNichtEnthaltenTestNotNull(){
        //Given
        int[] testArray = new int[]{1, 2, 3, 2, 1};
        int zahl = 99;

        //When
        boolean result = ArrayUebungen.istEnthalten(testArray, zahl);

        //Then
        assertFalse(result);
    }

    @Test
    public void istNichtEnthaltenTestNull(){
        //Given
        int[] testArray = null;
        int zahl = 99;

        //When and Then
        assertThrows(IllegalArgumentException.class, () -> ArrayUebungen.istEnthalten(testArray, zahl));
    }
}
