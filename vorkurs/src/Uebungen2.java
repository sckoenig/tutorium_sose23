import java.util.Scanner;

public class Uebungen2 {


    public static void main(String[] args) {
    }

    public static double Aufgabe5(double zahl1, double zahl2){
        return zahl1 * zahl2;
    }

    public static double Aufgabe6(int zahl1, int zahl2, int zahl3){
        return (zahl1+zahl2+zahl3) / 3.0;
    }

    public static double Aufgabe7(int n){
        double summe = 0;
        for (int i = 0; i <= n; i++) {
            summe+=i;
        }
        return summe/n;
    }

    public static void Aufgabe8(int n){
        for (int i = 1; i <= n; i+=2){
            System.out.println(i);
        }
    }

    public static void Aufgabe9(int n){
        for (int i = 1; i <= n; i++){
            System.out.println(i*i);
        }
    }

    public static void Aufgabe10(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Gib eine Zahl ein und ich gebe dir das Quadrat.");
        int eingabe = scanner.nextInt();
        System.out.println("Quadrat: " + eingabe * eingabe);
    }

    public static void Aufgabe11(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Gib zwei Zahlen ein");
        int eingabe1 = scanner.nextInt();
        int eingabe2 = scanner.nextInt();
        System.out.println("Summe: " + (eingabe1 + eingabe2));
    }

    public static void Aufgabe12(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wie viele Zahlen möchtest du eingeben?");
        int anzahl = scanner.nextInt();
        System.out.println("Tippe " + anzahl + " Zahlen ein.");

        double summe = 0;
        int eingabe = 0;
        for (int i = 0; i < anzahl; i++) {
            eingabe = scanner.nextInt();
            summe += eingabe;
        }
        System.out.println("Durchschnitt: "+ summe / anzahl);
    }

}
