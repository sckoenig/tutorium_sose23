import java.util.Arrays;
import java.util.Scanner;

public class ArrayUebungen {

    public static void main(String[] args) {

        //Aufgabe 2
        String[] tiere = new String[] {"Hund", "Katze", "Maus"};

        //Aufgabe 3
        tiere[0] = "Pferd";

        //Aufgabe 4
        for (int i = 0; i < tiere.length; i++) {
            System.out.println(tiere[i]);
        }

        int[] zahlenArray = new int[] {1,3,4,10};
        int summe = summeAusAllenZahlen(zahlenArray); // hier 18
        System.out.println(summe);

        Aufgabe6();

        int[] nochEinZahlenArray = new int[] {1, 4, 5, 4, 6, 10, 44, 4, 2};
        int anzahl = vorkommen(nochEinZahlenArray, 4); // hier 3
        System.out.println(anzahl);


    }

    public static int summeAusAllenZahlen(int[] array){
        int summe = 0;
        for (int i = 0; i < array.length; i++) {
            summe += array[i];
        }
        return summe;
    }

    public static void Aufgabe6(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wie viele Elemente enthält deine Liste?");
        int anzahl = scanner.nextInt(); //.nextInt() springt nicht in die nächste Zeile
        scanner.nextLine(); //spring in die nächste Zeile

        String[] einkaufsListe = new String[anzahl];
        System.out.println("Bitte gib deine Elemente ein:");

        for (int i = 0; i < anzahl; i++) {
            einkaufsListe[i] = scanner.nextLine();
        }
        System.out.println("Deine Liste: " + Arrays.toString(einkaufsListe));
    }

    public static int vorkommen(int[] array, int zahl){
        int zaehler = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == zahl){
                zaehler+=1;
            }
        }
        return zaehler;
    }
}
