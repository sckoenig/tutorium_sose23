import java.util.Scanner;

public class Bedingung {

    public static void main(String[] args) {
        primfaktoren(30);
        Scanner scanner = new Scanner(System.in);
        int summe = 0;

        while(summe < 100){
            int input = scanner.nextInt();
            summe += input;
            System.out.println(summe);
        }

        int zahl1 = 10;
        int zahl2 = 29;
        int zahl3 = 30;

        boolean a = zahl1 <= 10;
        boolean b = zahl1 >= zahl2;
        boolean c = zahl3 == zahl2;
        boolean d = zahl3 == zahl2+1;
        boolean e = zahl1 != zahl3;
        boolean f = zahl1 < zahl2 && zahl1 < zahl3;
        boolean g = zahl1 < zahl2 || zahl1 > zahl3;
        boolean h = zahl1 == zahl2 || zahl1 < zahl3;
        boolean i = zahl1 < zahl2 && zahl1 > zahl3;
        boolean j = !(zahl1 < zahl2);

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);
        System.out.println(g);
        System.out.println(h);
        System.out.println(i);
        System.out.println(j);


        boolean werktag = true;
        boolean ichHabUrlaub = true;

        if (werktag && !ichHabUrlaub){
            arbeitenGehen();
        } else {
            freizeit();
        }

        if (ichHabUrlaub || !werktag){
            freizeit();
        } else {
            arbeitenGehen();
        }
    }

    private static void freizeit() {
        System.out.println("Wir haben frei!");
    }

    private static void arbeitenGehen() {
        System.out.println("Wir müssen arbeiten!");
    }


    public static int findSmallestDivisor(int number) {
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) {
                return i; // i is a divisor of number, so return it
            }
        }
        return number; // if no divisor is found, the number itself is prime, so return it
    }

    public static void primfaktoren(int x){

        int teiler = x;
        while (true){
            int y = findSmallestDivisor(teiler);
            teiler /= y;
            System.out.println(y);
        }

    }
}
