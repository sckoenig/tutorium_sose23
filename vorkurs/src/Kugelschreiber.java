public class Kugelschreiber {

    int[] zahlenArray = new int[5];

    Mine mine;
    boolean geklickt;

    public Kugelschreiber(Mine mine) {
        this.mine = mine;
        this.geklickt = false; //Mine ist bei einem neuen Kugelschreiber versteckt
    }

    //wir können erst schreiben, wenn die Mine draußen ist und Tinte vorhanden
    //beim schreiben reduziert sich der Füllstand.
    public void schreiben(String text){
        if (geklickt){
            mine.tinteAufPapierÜbertragen(text);
        }
    }

    public void klicken(){
        this.geklickt = true;
    }

    public void mineWechseln(Mine neueMine){
        this.mine = neueMine;
    }

    public static void main(String[] args) {
        Mine mine = new Mine("schwarz", 100, 3);
        Kugelschreiber kugelschreiber = new Kugelschreiber(mine);
        kugelschreiber.klicken();
        kugelschreiber.schreiben("Liebes Tagebuch ...");

        Mine neueMine = new Mine("blau", 100, 2);
        kugelschreiber.mineWechseln(neueMine);

    }

}
