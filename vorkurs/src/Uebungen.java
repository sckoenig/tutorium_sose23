public class Uebungen {

    public static void main(String[] args) {
        System.out.println("Aufgabe 3: ");
        Aufgabe3("Anna");

        String name = "Anna Schmidt";
        int alter = 21;
        double notenschnitt = 12.5;

        System.out.println("Aufgabe 5: ");
        Aufgabe5(name, alter, notenschnitt);
        Aufgabe5("Christian Müller", 23, 1.3);

        System.out.println("Aufgabe 6: ");
        int ergebnis = Aufgabe6(10);
        System.out.println(ergebnis);

        System.out.println("Aufgabe 7: ");
        Aufgabe7(3);

        System.out.println("Aufgabe 8: ");
        Aufgabe8(10);

    }

    public static void Aufgabe3(String name) {
        System.out.println("Hallo, " + name + "!");
    }

    public static void Aufgabe5(String name, int alter, double schnitt) {
        System.out.println(name + ", " + alter + ", Notendurchschnitt: " + schnitt);
    }

    public static int Aufgabe6(int n) {
        int summe = 0;
        for (int i = 0; i < n; i++) {
            summe = summe + i;
            //oder: summe += i;
        }
        return summe;
    }

    public static void Aufgabe7(int n){
        for (int i = n; i >= 0; i--) { //i-- ist das gleiche wie i=i-1
            System.out.println(i);
        }
    }

    public static void Aufgabe8(int n){
        for (int i = 0; i <= n; i+=2) { //i wird jedes mal um 2 hochgezählt
            System.out.println(i);
        }
    }

}
