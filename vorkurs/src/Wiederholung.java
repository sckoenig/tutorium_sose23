import java.util.Scanner;

public class Wiederholung {

    public static void main(String[] args) {
        System.out.println("Aufgabe 7:");
        Aufgabe7();

        System.out.println();

        System.out.println("Aufgabe 8:");

        int[] array = new int[]{1,2,3};
        double d = durchschnitt(array); //2.0
        System.out.println(d);

    }

    public static void Aufgabe7(){
        System.out.println("Alle Zahlen, die du eingibst, werden summiert, bis du eine negative Zahl eingibst.");
        Scanner scanner = new Scanner(System.in);
        int eingabe = scanner.nextInt();
        int summe = 0;

        while(eingabe >= 0){
            summe += eingabe;
            eingabe = scanner.nextInt();
        }
        System.out.println("Summe: " + summe);
    }

    public static double durchschnitt(int[] array){
        double summe = 0;
        for (int i = 0; i < array.length; i++) {
            summe += array[i];
        }
        return summe/array.length;
    }

}
