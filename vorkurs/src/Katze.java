public class Katze {

    String name;
    int alter;

    public Katze(String name, int alter){
        this.name = name;
        this.alter = alter;
    }

    public void miau(){
        System.out.println(name + " miaut.");
    }

    public void altern(){
        this.alter+=1;
    }

    public static void main(String[] args) {
        Katze maya = new Katze("Maya", 4);
        Katze luna = new Katze("Luna", 12);
        maya.miau();
        luna.miau();
    }

}
