public class Mine {

    String farbe;
    int füllStand;
    int spitzenGröße;

    public Mine(String farbe, int fuellStand, int spitzenGröße) {
        this.farbe = farbe;
        this.füllStand = fuellStand;
        this.spitzenGröße = spitzenGröße;
    }

    public void tinteAufPapierÜbertragen(String text){
        if (füllStand > 0){
            System.out.println(text);
            this.füllStand -= 1;
        }
    }
}
