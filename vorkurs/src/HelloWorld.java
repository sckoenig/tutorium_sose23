public class HelloWorld {

    public static void main(String[] args) {
        helloWorld();
        druckeText("Hallo!");
        druckeAlleZahlenBisN(10);
        double summe = add(10, 5, 10.5);
        System.out.println(summe);
        System.out.println(add(10, 5, 10.5) - add(5, 5, 5.5));
        String text = "Ich bin eine Kette an Zeichen";
        System.out.println(text);
    }

    /**
     * Eine Methode, die alle Zahlen von 0 bis n addiert.
     */
    public static int add(int n){
        int summe = 0;
        for (int i = 1; i <= n; i++) {
            summe += i;
        }
        return summe;
    }

    /**
     * Eine Methode, die kein Ergebnis hat (void) und "Hello World"
     * auf die Konsole druckt.
     */
    public static void helloWorld(){
        System.out.println("Hello World!");
    }

    /**
     * Eine Methode, die kein Ergebnis hat (void) und alles
     * druckt, was man ihr übergibt
     */
    public static void druckeText(String text){
        System.out.println(text);
    }

    /**
     * Eine Methode mit Ergebnis vom Typ double.
     * Addiert drei Zahlen und gibt die Summe als Ergebnis zurück.
     */
    public static double add(int a, int b, double d){
        return a+b+d;
    }

    /**
     * EIne Methode, die kein Ergebnis hat (void).
     * Sie druckt alle Zahlen von 0 bis n.
     */
    public static void druckeAlleZahlenBisN(int n){
        for (int i = 0; i <= n; i++) {
            System.out.println(i);
        }
    }

}
